package main;

import static org.junit.Assert.*;

import model.Administrador;
import model.Moderador;
import model.Usuario;

import org.junit.Test;

import controller.ControleUsuario;

public class TesteJunit {
	
	@Test
	public void test() {
		Usuario novoUsuario = new Usuario();
		Moderador novoModerador = new Moderador();
		ControleUsuario novoControle = new ControleUsuario();
		Administrador novoAdministrador = new Administrador();
		
		novoUsuario.setNome("Joao");
		novoControle.adicionarUsuario(novoUsuario);
		
		novoUsuario.setNome("Pedro");
		novoControle.adicionarUsuario(novoUsuario);
		
		novoModerador.setNome("Carlos");
		novoControle.adicionarUsuario(novoModerador);
		
		novoAdministrador.setNome("Lucas");
		novoControle.adicionarUsuario(novoAdministrador);
		
		//Cria um novo moderador a partir de um usuario.
		novoModerador = novoAdministrador.adicionaModerador(novoUsuario);
		//Busca um usuario para depois removê-lo
		novoUsuario = novoControle.buscarUsuario(novoModerador.getNome());
		//Remove esse usuario
		novoControle.removeUsuario(novoUsuario);
		//Depois adiciona a lista de usuarios esse novo moderador
		novoControle.adicionarUsuario(novoModerador);
		
	}

}
