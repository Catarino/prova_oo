package controller;

import java.util.ArrayList;

import model.Administrador;
import model.Moderador;
import model.Usuario;

public class ControleUsuario {
	private ArrayList<Usuario> listaUsuario;
	
	public ControleUsuario(){
		listaUsuario = new ArrayList<Usuario>();
	}
	public void adicionarUsuario(Usuario umUsuario){
		listaUsuario.add(umUsuario);
	}
	public void adicionarUsuario(Moderador umUsuario){
		listaUsuario.add(umUsuario);
	}
	public void adicionarUsuario(Administrador umUsuario){
		listaUsuario.add(umUsuario);
	}
	public Usuario buscarUsuario(String umNome){
		for(Usuario umUsuario : listaUsuario){
			if(umUsuario.getNome().equalsIgnoreCase(umNome))
				return umUsuario;
		}
		return null;
	}
	public Moderador buscarModerador(String umNome){
		for(Usuario umModerador : listaUsuario){
			if(umModerador.getNome().equalsIgnoreCase(umNome))
				return (Moderador) umModerador;
		}
		return null;
	}
	public void removeUsuario(Usuario umUsuario){
		listaUsuario.remove(umUsuario);
	}
	public void visualizarPost(){
		
	}
	public void curtirPost(){
		
	}
}
