package model;

import java.util.ArrayList;

public class Usuario {
	//Atributos
	private String nome;
	private String cpf;
	private String email;
	private String senha;
	private int idade;
	private String telefone;
	private ArrayList<String> listaPost;
	private char posicaoCopa;
	private char tipoUsuario = 'C';
	//Construtores
	public Usuario(){
		listaPost = new ArrayList<String>();
	}
	public Usuario(String nome, String cpf, String email, String senha, int idade, String telefone, ArrayList<String> listaPost) {
		this.nome = nome;
		this.cpf = cpf;
		this.email = email;
		this.senha = senha;
		this.idade = idade;
		this.telefone = telefone;
		this.listaPost = new ArrayList<String>();
	}
	//Métodos
	public void setUsuario(){
		
	}
	public Usuario setUsuario(Usuario umUsuario){
		setNome(nome);
		setCpf(cpf);
		setEmail(email);
		setSenha(senha);
		setIdade(idade);
		setTelefone(telefone);
		setListaPost(listaPost);
		setPosicaoCopa(posicaoCopa);
		 setTipoUsuario(tipoUsuario);
		return umUsuario;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public void adicionaPost(String nomePost){
		listaPost.add(nomePost);
	}
	public void removePost(String nomePost){
		listaPost.remove(nomePost);
	}
	public ArrayList<String> getListaPost() {
		return listaPost;
	}
	public void setListaPost(ArrayList<String> listaPost) {
		this.listaPost = listaPost;
	}
	public char getPosicaoCopa() {
		return posicaoCopa;
	}
	public void setPosicaoCopa(char posicaoCopa) {
		this.posicaoCopa = posicaoCopa;
	}
	public char getTipoUsuario() {
		return tipoUsuario;
	}
	public void setTipoUsuario(char tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}
}
