package model;

import java.util.ArrayList;

public class Moderador extends Usuario{
	
	public Moderador(){
		super();
	}
	public Moderador(String nome, String cpf, String email, String senha, int idade, String telefone, ArrayList<String> listaPost){
		super(nome,cpf,email,senha,idade,telefone,listaPost);
		setTipoUsuario('M');
	}
	void removerUsuario(String nomeUsuario){
		getListaPost().remove(nomeUsuario);
	}
}
