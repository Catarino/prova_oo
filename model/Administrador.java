package model;

import java.util.ArrayList;

public class Administrador extends Moderador{
	public Administrador(){
		super();
	}
	public Administrador(String nome, String cpf, String email, String senha, int idade, String telefone, ArrayList<String> listaPost){
		super(nome,cpf,email,senha,idade,telefone,listaPost);
	}
	public Usuario removeModerador(Moderador umModerador){
		Usuario umUsuario = new Usuario();
		umModerador.setTipoUsuario('C');
		umUsuario.setNome(umModerador.getNome());
		umUsuario.setCpf(umModerador.getCpf());
		umUsuario.setEmail(umModerador.getEmail());
		umUsuario.setIdade(umModerador.getIdade());
		umUsuario.setSenha(umModerador.getSenha());
		umUsuario.setTelefone(umModerador.getTelefone());
		umUsuario.setListaPost(umModerador.getListaPost());
		umUsuario.setTipoUsuario(umModerador.getTipoUsuario());
		return umUsuario;
	}
	public Administrador adicionaAdministrador(Moderador umModerador){
		Administrador umAdministrador = new Administrador();
		umAdministrador.setTipoUsuario('C');
		umAdministrador.setNome(umModerador.getNome());
		umAdministrador.setCpf(umModerador.getCpf());
		umAdministrador.setEmail(umModerador.getEmail());
		umAdministrador.setIdade(umModerador.getIdade());
		umAdministrador.setSenha(umModerador.getSenha());
		umAdministrador.setTelefone(umModerador.getTelefone());
		umAdministrador.setListaPost(umModerador.getListaPost());
		umAdministrador.setTipoUsuario(umModerador.getTipoUsuario());
		return umAdministrador;
	}
	public Moderador adicionaModerador(Usuario umUsuario){
		Moderador umModerador = new Moderador();
		umModerador.setTipoUsuario('C');
		umModerador.setNome(umUsuario.getNome());
		umModerador.setCpf(umUsuario.getCpf());
		umModerador.setEmail(umUsuario.getEmail());
		umModerador.setIdade(umUsuario.getIdade());
		umModerador.setSenha(umUsuario.getSenha());
		umModerador.setTelefone(umUsuario.getTelefone());
		umModerador.setListaPost(umUsuario.getListaPost());
		umModerador.setTipoUsuario(umUsuario.getTipoUsuario());
		return umModerador;
	}
}
